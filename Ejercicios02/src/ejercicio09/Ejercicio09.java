package ejercicio09;

import java.util.Scanner;

public class Ejercicio09 {

	public static void main(String[] args) {
		Scanner escanner = new Scanner(System.in);
		
		System.out.println("Dame un numero de 5 cifras");
		int numero = escanner.nextInt();
		
		System.out.println(numero%10);
		System.out.println(numero%100);
		System.out.println(numero%1000);
		System.out.println(numero%10000);
		System.out.println(numero%100000);
		
		escanner.close();

	}

}
