package ejercicio08;

import java.util.Scanner;

public class Ejercicio08 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		
		System.out.println("Dame un numero (5 cifras)");
		int numero =input.nextInt();
		System.out.println(numero/10000);
		System.out.println(numero/1000);
		System.out.println(numero/100);
		System.out.println(numero/10);
		System.out.println(numero);
		
		System.out.println("Dame nuevamente un numero (5 cifras)");
		String cadena =input.nextLine();
		System.out.println(cadena.charAt(0));
		System.out.println(cadena.substring(0, 2));
		System.out.println(cadena.substring(0, 3));
		System.out.println(cadena.substring(0, 4));
		System.out.println(cadena);
		
		input.close();
		
	}

}
