package ejercicio05;

import java.util.Scanner;

public class Ejercicio05 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner escanner = new Scanner(System.in);
		
		System.out.println("Introduce una cadena");
		String cadena1=escanner.nextLine();
		
		System.out.println("Introduce otra cadena");
		String cadena2=escanner.nextLine();
		
		System.out.println(cadena1.equals(cadena2)?"Las cadenas son iguales"
				:"Las cadenas no son iguales");
			escanner.close();
	}

}
