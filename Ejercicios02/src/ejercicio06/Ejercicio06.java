package ejercicio06;

import java.util.Scanner;

public class Ejercicio06 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		System.out.println("Dime el cateto1 del triangulo");
		double cateto1=input.nextDouble();
		System.out.println("Introduce la longitud del cateto2");
		double cateto2=input.nextDouble();
		
		double hipotenusa=Math.sqrt(cateto1*cateto1)+(cateto2*cateto2);
		System.out.println("La hipotensusa es" +hipotenusa);
		input.close();

	}

}
