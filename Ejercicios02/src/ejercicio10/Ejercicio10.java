package ejercicio10;

import java.util.Scanner;

public class Ejercicio10 {

	public static void main(String[] args) {
		Scanner escanner = new Scanner(System.in);
		
		System.out.println("Introduce el dia de la fecha de nacimiento de una persona");
		int dia = escanner.nextInt();
		
		System.out.println("Introduce el mes de la fecha de nacimiento de una persona");
		int mes = escanner.nextInt();
		
		System.out.println("Introduce el a�o de la fecha de nacimiento de una persona");
		int anno = escanner.nextInt();
		
		int suma = dia + mes + anno;
		int sumaTotal=0;
		
		//saco el valor de cada cifra por separado
		sumaTotal=sumaTotal+(suma/1000);
		sumaTotal=sumaTotal+(suma%10);
		sumaTotal=sumaTotal+((suma/100)%10);
		sumaTotal=sumaTotal+(suma%100)/10;
		
		System.out.println("El numero de la suerte es "+sumaTotal);
		
		// muestro cifras (no es necesario) solo para comprobar
		System.out.println(suma / 1000);
		System.out.println(suma % 10);
		System.out.println((suma / 100) % 10);
		System.out.println((suma % 100) / 10);
		
		escanner.close();
		

	}

}
