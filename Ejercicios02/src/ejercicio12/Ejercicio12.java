package ejercicio12;

import java.util.Scanner;

public class Ejercicio12 {

	public static void main(String[] args) {
		Scanner escanner = new Scanner(System.in);
		
		System.out.println("Introduce una cadena de texto");
		String cadena = escanner.nextLine();
		
		System.out.println("Introduce un caracter");
		char caracter=escanner.nextLine().charAt(0);
		
		System.out.println(caracter==cadena.charAt(0)?
				"la cadena comienza por el caracter"
				:"La cadena no comienza por el caracter");
		escanner.close();
	}

}
