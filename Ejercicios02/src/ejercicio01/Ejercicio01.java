package ejercicio01;

import java.util.Scanner;

public class Ejercicio01 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		System.out.println("dame un numero entero");
		int numInt=input.nextInt();
		System.out.println("El doble del numero es " +numInt*2);
		System.out.println("El triple del numero es " +numInt*3);
		
		input.close();

	}

}
