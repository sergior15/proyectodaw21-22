package ejercicio13;

import java.util.Scanner;

public class Ejercicio13 {

	public static void main(String[] args) {
		Scanner escanner = new Scanner(System.in);
		
		System.out.println("Introduce una cadena");
		String cadena = escanner.nextLine();
		
		System.out.println("Introduce otra cadena");
		String cadena2 = escanner.nextLine();
		
		System.out.println(cadena2.contains(cadena)?
				"Si esta contenida"
				:"No esta contenida");
				
		
		escanner.close();

	}

}
