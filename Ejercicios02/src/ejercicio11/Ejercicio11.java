package ejercicio11;

import java.util.Scanner;

public class Ejercicio11 {

	public static void main(String[] args) {
		Scanner escanner = new Scanner(System.in);
		
		System.out.println("Dame una cadena");
		String cadena =escanner.nextLine();
		
		boolean contieneVocales=cadena.contains("a") ||cadena.contains("e")||cadena.contains("i")||cadena.contains("o")||cadena.contains("u");
		
		System.out.println(contieneVocales?"contiene vocales":"no tiene vocales");
		
		boolean empiezaPorVocal=cadena.startsWith("a")||cadena.startsWith("e")||cadena.startsWith("i")||cadena.startsWith("o")||cadena.startsWith("u");
		
		System.out.println(empiezaPorVocal ? "empieza por vocal" : "no empieza por vocal");
		
		boolean terminaPorVocal=cadena.endsWith("a")||cadena.endsWith("e")||cadena.endsWith("i")||cadena.endsWith("o")||cadena.endsWith("u");
		
		System.out.println(terminaPorVocal ? "termina por vocal" : "no termina por vocal");
		
		escanner.close();
	}

}
