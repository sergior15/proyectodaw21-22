package ejercicio14;

public class Ejercicio14 {

	public static void main(String[] args) {
		String textoEntero = "12345";
		String textoEnteroLargo= "234455767";
		String textoDecimal = "1234.2255";
		String textoByte="123";
		
		//transformamos un texto que contiene un entero a un entero
		int entero=Integer.parseInt(textoEntero);
		//transformamos un texto que contiene un long a un long
		long enteroLargo=Long.parseLong(textoEnteroLargo);
		//transformamos un texto que contiene
		double enteroDecimal= Double.parseDouble(textoDecimal);

		
		System.out.println(entero);
		System.out.println(enteroLargo);
		System.out.println(textoDecimal);
		System.out.println(textoByte);
	}

}
