package ejercicio18;

import java.util.Scanner;

public class Ejercicio18 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		System.out.println("dame un numero entero");
		int numInt=input.nextInt();
		System.out.println("El numero entero es" +numInt);
		
		System.out.println("dame un numero byte");
		byte numByte=input.nextByte();
		System.out.println("El numero byte es" +numByte);
		
		System.out.println("dame un numero short");
		short numShort=input.nextShort();
		System.out.println("El numero short es" +numShort);
		
		System.out.println("dame un numero long");
		long numLong=input.nextLong();
		System.out.println("El numero long es" +numLong);
		
		System.out.println("dame un numero float");
		float numFloat=input.nextFloat();
		System.out.println("El numero float es" +numFloat);
		
		System.out.println("dame un numero double");
		double numDouble=input.nextDouble();
		System.out.println("El numero double es" +numDouble);
		
		//limpio buffer (he leido un numero y ahora leo un caracter)
		input.nextLine();
		
		//char 
		//para leer un caracter se usa nextLine que lee una linea 
		//no hay un nextChar se usa nextLine
		//para guardar solo el primer caracter se tiene que indicar con .charAT(0)
		System.out.println("Introduce un char");
		char caracter=input.nextLine().charAt(0);
		System.out.println("El caracter es "+caracter);
		
		input.close();

	}

}
