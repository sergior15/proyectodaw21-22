package ejercicio05;

public class Ejercicio05 {

	public static void main(String[] args) {
		int a=5;
		int b=3;
		int c=-12;
		
		System.out.println(a>3);//true
		System.out.println(a>c);//true
		System.out.println(a<c);//false
		System.out.println(b<c);//false
		System.out.println(b!=c);//true
		System.out.println(a*b==15);//true
		System.out.println(c/b==-4);//true
		System.out.println(c/b<a);//true
		System.out.println(c/b==-10);//false
		System.out.println(a%b==2);//true
		System.out.println(a+b+c==5);//true
		System.out.println((a+b==8)&&(a-b==2));//false
		System.out.println((a+b==8)||(a-b==6));//true
		System.out.println(a>3&&b>3&&c<3);
		System.out.println(a>3&&b>=3&&c<-3);
	
	}

}
