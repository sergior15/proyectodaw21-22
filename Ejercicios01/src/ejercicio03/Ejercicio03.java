package ejercicio03;

public class Ejercicio03 {

	public static void main(String[] args) {
		int num = 5;
		// incrementar en 77
		num = num + 77;
		System.out.println(num);

		// decrementar en 3
		num -= 3;
		System.out.println(num);

		// duplicar el valor
		num = num * 2;
		System.out.println(num);

	}

}
