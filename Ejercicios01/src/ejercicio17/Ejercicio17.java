package ejercicio17;

public class Ejercicio17 {

	public static void main(String[] args) {
		String cadena1="n";
		String cadena2="Otra cadena";
		
		System.out.println(cadena1.length()==cadena2.length()?"son iguales":
				(cadena1.length()>cadena2.length())?"cadena1 es mayor que cadena 2":
					"cadena1 es menor de cadena2");
		
		//Los String tienen un metodo que permite comparar cadenas
		//compareTO
		//el resultado de compareTO es un entero
		//primera Cadena.compareTO(segundaCadena) -> devuelve un entero
		
		int resultado=cadena1.compareTo(cadena2);
		
		//resultado>0 -> cadena1>cadena2
		//resultado<0 -> cadena1<cadena2
		//resultado=0 -> cadena1=cadena2
		//compara los codigos ascii
		
		System.out.println(resultado>0?"cadena2 es mayor":
			(resultado==0?"cadena1 es igual a cadena2":"cadena1 es mayor"));
		
		
		
	}

}
