package ejercicio01;

public class Ejercicio01 {

	public static void main(String[] args) {
		// declaro 3 variables y las inicializo
		int entero = 5;
		double decimal = 2.69;
		char caracter = 'Y';
		
		//muestro su valor por pantalla
		System.out.println(entero);
		System.out.println(decimal);
		System.out.println(caracter);

		//sumo el int con el double
		System.out.println(entero+decimal);
		//resto el int double
		System.out.println(entero-entero);
		
		//muestro el valor del caracter
		System.out.println((int)caracter);
		
	}

}
