package ejercicio08;

public class Ejercicio08 {

	public static void main(String[] args) {
		int varA = -783;
		
		System.out.println(varA%2==1?"Impar":"par");
		System.out.println(varA%2==0 && varA%3==0?"es multiplo de 2 y de 3":"no lo es");
	}

}
