package ejercicio12;

public class Ejercicio12 {

	public static void main(String[] args) {
		double decimalDoble=5.2345223E150;
		float decimalSimple=(float)decimalDoble;//explicita
		
		System.out.println(decimalDoble);
		System.out.println(decimalSimple);

		System.out.println("he convertido un decimal de 8 bytes");
		System.out.println("a uno de 4 bytes");
	}

}
