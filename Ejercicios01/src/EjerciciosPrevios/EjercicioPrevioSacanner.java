package EjerciciosPrevios;

import java.util.Scanner;

public class EjercicioPrevioSacanner {

	public static void main(String[] args) {
		
		//creo un objeto Scanner llamado input
		Scanner input = new Scanner(System.in);
		
		//vamos a usarlo
		System.out.println("dame un numero entero");
		//el metodo nextInt() permite leer un entero
		int numInt=input.nextInt();
		//lo muestro
		System.out.println("El numero entero es "+numInt);
		
		//ciero el Scanner
		input.close();

	}

	
		
	}


