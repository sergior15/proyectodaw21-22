package EjerciciosPrevios;

public class Ejerciciosprevios {

	public static void main(String[] args) {
		// Declaro una variable entera var1 y le doy un valor, lo muestro junto con un
		// texto
		int var1 = 5;
		System.out.println("El valor de var1 es" + var1);

		// Declaro una variable double var2 y le doy un valor, lo mustro junto con un
		// texto
		double var2 = 12.123434;
		System.out.println("El valor de var2 es" + var2);

		// Declaro una varibale char var3 y le doy una letra, muestro su valor entero
		char var3 = 't';
		System.out.println("El valor entero de var3 es" + (int) var3);

		// Declaro una variable var4 y le incremento uno, muestro ambas variables
		int var4 = 6;
		var4++;
		System.out.println("El valor de var4 es" + var4);

		// Declaro una variable double var5 y la casteo a entera
		double var5 = 34.984756782;
		System.out.println("El valor de var5 es" + var5);
		int var5Entera = (int) var5;
		System.out.println("El valor de va5Entera es" + var5Entera);

		// Declaro una variable double var6 que contenga la suma de var2 y var3
		double var6 = var2 + var3;
		System.out.println("El valor de var6 es" + var6);

		// Declaro una variable double var7 que contenga la divisi�n double de var4 y
		// var5
		double var7 = (double) var4 / var5;
		System.out.println(var7);

		// Declaro un boolean var8 y lo inicializo a true, muestro su valor con un texto
		boolean var8 = true;
		System.out.println("El valor de condicion es " + var8);

		// Declaro tres numeros, num1=3, num2=5, num3=6
		int num1 = 3;
		int num2 = 5;
		int num3 = 6;

		// Creo un operador condicional que use num1 y num2 y muestre true (con el
		// operador AND)
		System.out.println((num1>=2) && (num2<=4)?"num1 es >02 y num2 <=4" : "no cumple condiciones");
		
		// Creo un operador condicional que use num2 y num3 y muestre false (con el
		// operador OR)
		System.out.println((num2==3 || (num3<=5)?"num2 no es igual a 3 ni num3 es >=5":"no cumple las condiciones"));
		
		// Muestro mediante un syso y un operador condicional la comprobaci�n de que
		// num3 es par
		System.out.println((num3%2==0)?"es par":"no es par");
		
		// Decrementa la variable num2 2 unidades, muestrala
		System.out.println("Valor antes de decremento" +num2);
		num2-=2;
		System.out.println("Valor despu�s de decremento" +num2);

	}

}
