package EjerciciosPrevios;

public class EjerciciospreviosString {

	public static void main(String[] args) {
		int num1 = 3; // tipo primitivo
		System.out.println("El valor de num1 es " + num1);
		char letra = 'a'; // tipo primitivo
		System.out.println("La letra es " + letra);
		String cadena = "hola caracola";// No es primitivo --> Mayusculas --> clase
		System.out.println("La cadena es" + cadena);
		// Clase String pertenece a java.lang
		// java.lang es paquete de java
		// viene por defecto, por eso no necesita import
		System.out.println("La longitud de la cadena es " + cadena.length());
		// Declaro un String y lo muestro
		String textoEntero = "1245";
		System.out.println(textoEntero);
		// declaro un entero y lo parseo a int usando una clase (integer)
		int entero = Integer.parseInt(textoEntero);
		System.out.println(entero);
		entero--;
		System.out.println(entero);
		//Los String tienen  muchas funcionalidades
		//Por ejemplo extraer un trozo de una cadena
		String frase="Esto es una frase completa";
		System.out.println(frase);;
		//Con susbstring le puedo indicar el comienzo 
		

	}

}
